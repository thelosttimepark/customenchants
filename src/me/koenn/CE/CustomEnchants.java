package me.koenn.CE;

import me.koenn.CE.commands.Enchanter;
import me.koenn.CE.commands.Enchants;
import me.koenn.CE.enchantments.EnchantType;
import me.koenn.CE.listeners.eventlisteners.BlockListener;
import me.koenn.CE.listeners.eventlisteners.DamageListener;
import me.koenn.CE.listeners.eventlisteners.DragAndDropListener;
import me.koenn.CE.listeners.eventlisteners.EquipListener;
import me.koenn.CE.logger.Logger;
import me.koenn.CE.registry.EnchantmentRegistry;
import me.koenn.CE.util.CraftingManager;
import me.koenn.CE.util.JSONUtil;
import me.koenn.CE.util.SoundUtil;
import me.koenn.core.command.CommandAPI;
import me.koenn.core.data.JSONManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Custom enchantments plugin version 2.0.
 * <p>
 * Copyright (C) Koen Willemse - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Koen Willemse, April 2016
 */
@SuppressWarnings("ResultOfMethodCallIgnored")
public class CustomEnchants extends JavaPlugin {

    public static Plugin instance;
    public static int[] expCost;
    private static boolean debugMode;
    private static EnchantmentRegistry registry;
    private static String version;

    /**
     * Get the enchantment registry
     *
     * @return EnchantmentRegistry instance
     */
    public static EnchantmentRegistry getEnchantmentRegistry() {
        return registry;
    }

    /**
     * See if the plugin is in debug mode
     *
     * @return boolean debugMode
     */
    public static boolean isInDebugMode() {
        return debugMode;
    }

    public static String getVersion() {
        return version;
    }

    /**
     * Gets called when the plugin enables.
     * Handles all registering of most things, and initializes all objects.
     */
    @Override
    public void onEnable() {
        //Setup static variables.
        instance = this;
        debugMode = false;

        //Check the Bukkit version and disable the plugin if its invalid.
        if (!checkVersion()) {
            Logger.info("######################################################");
            Logger.info("This plugin is only compatible with 1.8, 1.9 and 1.10!");
            Logger.info("######################################################");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        //Make the JSONManagers.
        try {
            Settings.colorSettings = new JSONManager(this, "colors.json", true, (JSONObject) new JSONParser().parse(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("colors.json"))));
            Settings.expSettings = new JSONManager(this, "exp.json", true, (JSONObject) new JSONParser().parse(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("exp.json"))));
            Settings.guiSettings = new JSONManager(this, "guis.json", true, (JSONObject) new JSONParser().parse(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("guis.json"))));
        } catch (IOException | ParseException e) {
            Logger.info("############################################################################");
            Logger.info("An error occurred while reading internal files, please report this to Koenn!");
            Logger.info("############################################################################");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        //Initialize the SoundUtil with the right version.
        SoundUtil.init(version);

        //Print startup messages.
        Logger.info("All credits for this plugin go to Koenn");
        if (debugMode) {
            Logger.debug("Loading plugin in debug mode!");
        }

        //Load the exp costs from the settings.
        expCost = new int[5];
        for (int i = 0; i < 5; i++) {
            System.out.println(EnchantType.values()[i].name().toLowerCase() + " = " + Settings.expSettings.getFromBody(EnchantType.values()[i].name()));
            expCost[i] = Integer.parseInt((String) Settings.expSettings.getFromBody(EnchantType.values()[i].name()));
        }

        //Setup enchantment registry and register all enchantments.
        registry = new EnchantmentRegistry();
        EnchantmentRegistry.registerEnchantments(registry);

        //Load in all the enchantment settings.
        Settings.enchantments = new JSONManager(this, "enchantments.json", true, JSONUtil.getEnchantmentsJSON());
        JSONUtil.readEnchantmentsFromFile();

        //Register all listeners.
        Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
        Bukkit.getPluginManager().registerEvents(new DragAndDropListener(), this);
        Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
        EquipListener.startEquipListener();

        //Register all commands.
        CommandAPI.registerCommand(new Enchanter(), this);
        CommandAPI.registerCommand(new Enchants(), this);

        //Register crafting recipes.
        CraftingManager.registerRecipes();
    }

    /**
     * Get the current Bukkit version of the server and check if its valid.
     */
    public boolean checkVersion() {
        //Check the version and set the version variable.
        String versionString = Bukkit.getServer().getBukkitVersion();
        if (versionString.contains("1.9")) {
            version = "1.9";
        } else if (versionString.contains("1.8")) {
            version = "1.8";
        } else if (versionString.contains("1.10")) {
            version = "1.10";
        } else {
            return false;
        }
        Logger.info("Loading plugin for minecraft version " + version + "!");
        return true;
    }

    /**
     * Gets called when the plugin disables.
     * Handles finishing things up.
     */
    @Override
    public void onDisable() {
        Logger.info("All credits for this plugin go to Koenn");
    }

    public static class Settings {
        public static JSONManager colorSettings;
        public static JSONManager expSettings;
        public static JSONManager guiSettings;
        public static JSONManager enchantments;
    }
}