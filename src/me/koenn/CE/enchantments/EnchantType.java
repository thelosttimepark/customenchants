package me.koenn.CE.enchantments;

import me.koenn.CE.CustomEnchants;
import org.bukkit.ChatColor;

public enum EnchantType {

    SIMPLE, COMMON, RARE, ULTRA_RARE, LEGENDARY;

    String string;

    EnchantType() {
        this.string = this.name().charAt(0) + this.name().substring(1).toLowerCase();
    }


    @Override
    public String toString() {
        return this.string;
    }

    public ChatColor getColor() {
        return ChatColor.valueOf((String) CustomEnchants.Settings.colorSettings.getFromBody(this.name()));
    }
}
