package me.koenn.CE.enchantments.catagories.armor;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.armorlisteners.ArmorEquippedListener;
import me.koenn.CE.references.Enchantments;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CureEnchant extends CustomEnchantment {

    public CureEnchant() {
        super(Enchantments.Cure.NAME, Enchantments.Cure.MAX_LEVEL, Enchantments.Cure.DESC, Enchantments.Cure.TOOL, Enchantments.Cure.TYPE);
        this.getListeners().add((ArmorEquippedListener) (player, level) -> {
            for (PotionEffect effect : player.getActivePotionEffects()) {
                if (effect.getType().equals(PotionEffectType.SLOW_DIGGING)) {
                    player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                }
                if (effect.getType().equals(PotionEffectType.CONFUSION)) {
                    player.removePotionEffect(PotionEffectType.CONFUSION);
                }
                if (effect.getType().equals(PotionEffectType.BLINDNESS)) {
                    player.removePotionEffect(PotionEffectType.BLINDNESS);
                }
            }
        });
    }
}
