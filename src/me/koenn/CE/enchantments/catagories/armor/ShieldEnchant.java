package me.koenn.CE.enchantments.catagories.armor;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.damagelisteners.MDamageTakeListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Chance;

public class ShieldEnchant extends CustomEnchantment {

    public ShieldEnchant() {
        super(Enchantments.Shield.NAME, Enchantments.Shield.MAX_LEVEL, Enchantments.Shield.DESC, Enchantments.Shield.TOOL, Enchantments.Shield.TYPE);
        this.getListeners().add((MDamageTakeListener) (event, damager, victim, level) -> {
            if (Chance.chanceOf(15 + (level * 5))) {
                event.setDamage(0);
            }
        });
    }
}
