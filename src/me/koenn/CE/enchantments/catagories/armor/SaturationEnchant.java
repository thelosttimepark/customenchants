package me.koenn.CE.enchantments.catagories.armor;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.armorlisteners.ArmorEquippedListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Util;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SaturationEnchant extends CustomEnchantment {

    public SaturationEnchant() {
        super(Enchantments.Saturation.NAME, Enchantments.Saturation.MAX_LEVEL, Enchantments.Saturation.DESC, Enchantments.Saturation.TOOL, Enchantments.Saturation.TYPE);
        this.getListeners().add((ArmorEquippedListener) (player, level) -> Util.applyEffect(new PotionEffect(PotionEffectType.SATURATION, 200, level - 1, true, false), player));
    }
}
