package me.koenn.CE.enchantments.catagories.armor;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.armorlisteners.ArmorEquippedListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Util;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class AntiFireEnchant extends CustomEnchantment {

    public AntiFireEnchant() {
        super(Enchantments.AntiFire.NAME, Enchantments.AntiFire.MAX_LEVEL, Enchantments.AntiFire.DESC, Enchantments.AntiFire.TOOL, Enchantments.AntiFire.TYPE);
        this.getListeners().add((ArmorEquippedListener) (player, level) -> Util.applyEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 200, 0, true, false), player));
    }
}
