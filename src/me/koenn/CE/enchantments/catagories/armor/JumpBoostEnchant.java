package me.koenn.CE.enchantments.catagories.armor;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.armorlisteners.ArmorEquippedListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Util;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class JumpBoostEnchant extends CustomEnchantment {

    public JumpBoostEnchant() {
        super(Enchantments.JumpBoost.NAME, Enchantments.JumpBoost.MAX_LEVEL, Enchantments.JumpBoost.DESC, Enchantments.JumpBoost.TOOL, Enchantments.JumpBoost.TYPE);
        this.getListeners().add((ArmorEquippedListener) (player, level) -> Util.applyEffect(new PotionEffect(PotionEffectType.JUMP, 200, level - 1, true, false), player));
    }
}
