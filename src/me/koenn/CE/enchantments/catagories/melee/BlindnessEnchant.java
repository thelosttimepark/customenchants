package me.koenn.CE.enchantments.catagories.melee;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.damagelisteners.MDamageDealListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Chance;
import me.koenn.CE.util.SoundUtil;
import org.bukkit.Sound;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class BlindnessEnchant extends CustomEnchantment {

    public BlindnessEnchant() {
        super(Enchantments.Blindness.NAME, Enchantments.Blindness.MAX_LEVEL, Enchantments.Blindness.DESC, Enchantments.Blindness.TOOL, Enchantments.Blindness.TYPE);
        this.getListeners().add((MDamageDealListener) (event, damager, victim, level) -> {
            if (Chance.chanceOf((level * 2) + 10)) {
                victim.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (level + 1) * 50, 0));
                Sound sound = SoundUtil.ORB_PICKUP;
                damager.playSound(damager.getLocation(), sound, 1F, 1.8F);
            }
        });
    }
}
