package me.koenn.CE.enchantments.catagories.general;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.armorlisteners.ArmorEquippedListener;
import me.koenn.CE.references.Enchantments;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class AutoRepairEnchantment extends CustomEnchantment {

    @SuppressWarnings("deprecation")
    public AutoRepairEnchantment() {
        super(Enchantments.AutoRepair.NAME, Enchantments.AutoRepair.MAX_LEVEL, Enchantments.AutoRepair.DESC, Enchantments.AutoRepair.TOOL, Enchantments.AutoRepair.TYPE);
        this.getListeners().add((ArmorEquippedListener) (player, level) -> {
            if (new Random().nextInt(10) > 1) {
                return;
            }
            for (ItemStack item : player.getInventory().getArmorContents()) {
                if (!isValidTool(item.getType())) {
                    return;
                }
                if (item.getDurability() > 0) {
                    item.setDurability(((short) (item.getDurability() + 1)));
                }
            }
            ItemStack item;
            if (CustomEnchants.getVersion().equals("1.8")) {
                item = player.getItemInHand();
            } else {
                item = player.getInventory().getItemInMainHand();
            }
            if (!isValidTool(item.getType())) {
                return;
            }
            if (item.getDurability() > 0) {
                item.setDurability(((short) (item.getDurability() - 1)));
            }
        });
    }

    private boolean isValidTool(Material tool) {
        String toolName = tool.name();
        return toolName.endsWith("HELMET") || toolName.endsWith("CHESTPLATE") || toolName.endsWith("LEGGINGS") || toolName.endsWith("BOOTS") || toolName.endsWith("PICKAXE") || toolName.endsWith("SHOVEL") || toolName.endsWith("SWORD") || toolName.endsWith("AXE") || toolName.equals("BOW");
    }
}
