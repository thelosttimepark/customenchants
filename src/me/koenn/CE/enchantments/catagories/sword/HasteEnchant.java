package me.koenn.CE.enchantments.catagories.sword;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.armorlisteners.ArmorEquippedListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Util;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HasteEnchant extends CustomEnchantment {

    public HasteEnchant() {
        super(Enchantments.Haste.NAME, Enchantments.Haste.MAX_LEVEL, Enchantments.Haste.DESC, Enchantments.Haste.TOOL, Enchantments.Haste.TYPE);
        this.getListeners().add((ArmorEquippedListener) (player, level) -> Util.applyEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 200, level - 1, true, false), player));
    }
}
