package me.koenn.CE.enchantments.catagories.axe;

import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.listeners.blocklisteners.BreakListener;
import me.koenn.CE.references.Enchantments;
import me.koenn.CE.util.Util;
import org.bukkit.Material;

public class LumberEnchantment extends CustomEnchantment {

    public LumberEnchantment() {
        super(Enchantments.Lumber.NAME, Enchantments.Lumber.MAX_LEVEL, Enchantments.Lumber.DESC, Enchantments.Lumber.TOOL, Enchantments.Lumber.TYPE);
        this.getListeners().add((BreakListener) (event, player, block, level) -> {
            if (!block.getType().equals(Material.LOG) && !block.getType().equals(Material.LOG_2)) {
                return;
            }
            Util.breakTreeFrom(block, player);
        });
    }
}
