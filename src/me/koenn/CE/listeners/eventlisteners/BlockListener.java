package me.koenn.CE.listeners.eventlisteners;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.listeners.EventManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockListener implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        /*ItemStack item = event.getItemInHand();
        if (!item.getType().equals(Material.ENCHANTMENT_TABLE)) {
            return;
        }
        if (event.getBlock().getLocation().clone().add(0, -1, 0).getBlock().getType().equals(Material.BEDROCK)) {
            event.setCancelled(true);
            return;
        }
        if (!item.hasItemMeta() || !item.getItemMeta().hasDisplayName()) {
            return;
        }
        if (!item.getItemMeta().hasItemFlag(ItemFlag.HIDE_ENCHANTS)) {
            return;
        }
        event.getBlockPlaced().getLocation().clone().add(0, -1, 0).getBlock().setType(Material.BEDROCK);
        TableEffect effect = new TableEffect(event.getBlock());
        effect.play();*/
    }

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (CustomEnchants.getVersion().equals("1.8")) {
            EventManager.callBlockBroken(player, event, player.getItemInHand());
        } else {
            EventManager.callBlockBroken(player, event, player.getInventory().getItemInMainHand());
            EventManager.callBlockBroken(player, event, player.getInventory().getItemInOffHand());
        }
        /*Block block = event.getBlock();
        if (!block.getType().equals(Material.ENCHANTMENT_TABLE)) {
            return;
        }
        if (!block.getLocation().add(0, -1, 0).getBlock().getType().equals(Material.BEDROCK)) {
            return;
        }
        block.getLocation().add(0, -1, 0).getBlock().setType(Material.DIRT);
        if (event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        event.setCancelled(true);
        block.setType(Material.AIR);
        Item item = block.getLocation().getWorld().dropItem(block.getLocation().add(0.5, 0.2, 0.5), CraftingManager.getEnhancedTable());
        item.setVelocity(item.getVelocity().zero());
        TableEffect effect = new TableEffect(event.getBlock());
        effect.play();*/
    }
}
