package me.koenn.CE.listeners.eventlisteners;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.effects.EnchantEffect;
import me.koenn.CE.enchantments.EnchantType;
import me.koenn.CE.util.BookUtil;
import me.koenn.CE.util.SoundUtil;
import me.koenn.CE.util.TableUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Listener for enchantment event.
 */
@Deprecated
public class EnchantListener implements Listener {

    /**
     * Listens for a player putting an item into the enchant table.
     *
     * @param event PrepareItemEnchantEvent
     */
    @EventHandler
    public void onPrepareItemEnchant(PrepareItemEnchantEvent event) {
        if (!event.getEnchantBlock().getLocation().clone().add(0, -1, 0).getBlock().getType().equals(Material.BEDROCK)) {
            return;
        }
        if (!TableUtil.isEnhancedTable(event.getEnchantBlock())) {
            event.setCancelled(true);
            return;
        }
        if (!event.getItem().getType().equals(Material.BOOK)) {
            event.setCancelled(true);
            return;
        }
        event.getExpLevelCostsOffered()[0] = 30;
        event.getExpLevelCostsOffered()[1] = 40;
        event.getExpLevelCostsOffered()[2] = 50;
    }

    /**
     * Listens for a player enchanting an item in the enchantment table.
     *
     * @param event EnchantItemEvent
     */
    @EventHandler
    public void onEnchantItem(EnchantItemEvent event) {
        if (!TableUtil.isEnhancedTable(event.getEnchantBlock())) {
            return;
        }
        ItemStack item = event.getItem();
        if (!item.getType().equals(Material.BOOK)) {
            return;
        }
        ItemStack enchantedBook = BookUtil.getBook(EnchantType.SIMPLE);
        ItemMeta meta = enchantedBook.getItemMeta();
        Bukkit.getScheduler().scheduleSyncDelayedTask(CustomEnchants.instance, () -> {
            item.setItemMeta(meta);
            item.setType(Material.BOOK);
        }, 1);

        Bukkit.getLogger().info(event.getExpLevelCost() + "");
        Bukkit.getScheduler().scheduleSyncDelayedTask(CustomEnchants.instance, () -> {
            switch (event.getExpLevelCost()) {
                case 30:
                    event.getEnchanter().setLevel(event.getEnchanter().getLevel() - 9);
                    break;
                case 40:
                    event.getEnchanter().setLevel(event.getEnchanter().getLevel() - 13);
                    break;
                case 50:
                    event.getEnchanter().setLevel(event.getEnchanter().getLevel() - 17);
                    break;
            }
        }, 1);

        EnchantEffect enchantEffect = new EnchantEffect(event.getEnchantBlock());
        enchantEffect.play();

        Sound enchantSound = SoundUtil.LEVEL_UP;
        Player player = event.getEnchanter();
        player.playSound(player.getLocation(), enchantSound, 1.0F, 0.2F);
    }
}
