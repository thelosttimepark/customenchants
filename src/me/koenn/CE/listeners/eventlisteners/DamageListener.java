package me.koenn.CE.listeners.eventlisteners;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.listeners.EventManager;
import me.koenn.CE.logger.Logger;
import org.bukkit.GameMode;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * Listener for all different damage events.
 * Calls all damage listeners for the enchantments using the EventManager
 *
 * @see EventManager
 */
public class DamageListener implements Listener {

    /**
     * Base listener for all EntityDamageByEntityEvent events.
     *
     * @param event EntityDamageByEntityEvent instance
     */
    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof LivingEntity)) {
            return;
        }
        if (!(event.getEntity() instanceof LivingEntity)) {
            return;
        }
        if (event.isCancelled()) {
            return;
        }
        Logger.debug("Damage {");
        LivingEntity damager = (LivingEntity) event.getDamager();
        LivingEntity victim = (LivingEntity) event.getEntity();
        if (damager instanceof Player) {
            if (victim instanceof Player) {
                if (((Player) victim).getGameMode().equals(GameMode.CREATIVE)) {
                    return;
                }
                Logger.debug("  PvP;");
                if (CustomEnchants.getVersion().equals("1.8")) {
                    EventManager.callPlayerDamageDeal((Player) damager, (Player) victim, event, ((Player) damager).getItemInHand());
                    EventManager.callPlayerMDamageDeal((Player) damager, victim, event, ((Player) damager).getItemInHand());
                } else {
                    EventManager.callPlayerDamageDeal((Player) damager, (Player) victim, event, ((Player) damager).getInventory().getItemInMainHand());
                    EventManager.callPlayerMDamageDeal((Player) damager, victim, event, ((Player) damager).getInventory().getItemInMainHand());

                    EventManager.callPlayerDamageDeal((Player) damager, (Player) victim, event, ((Player) damager).getInventory().getItemInOffHand());
                    EventManager.callPlayerMDamageDeal((Player) damager, victim, event, ((Player) damager).getInventory().getItemInOffHand());
                }
                EventManager.callPlayerDamageTake((Player) damager, (Player) victim, event);
            } else {
                Logger.debug("  PvE;");
                if (CustomEnchants.getVersion().equals("1.8")) {
                    EventManager.callPlayerMDamageDeal((Player) damager, victim, event, ((Player) damager).getItemInHand());
                } else {
                    EventManager.callPlayerMDamageDeal((Player) damager, victim, event, ((Player) damager).getInventory().getItemInMainHand());
                    EventManager.callPlayerMDamageDeal((Player) damager, victim, event, ((Player) damager).getInventory().getItemInOffHand());
                }
            }
        } else {
            if (victim instanceof Player) {
                if (((Player) victim).getGameMode().equals(GameMode.CREATIVE)) {
                    return;
                }
                Logger.debug("  EvP;");
                EventManager.callPlayerMDamageTake(damager, (Player) victim, event);
            }
        }
        Logger.debug("}");
    }

    /**
     * Base listener for all EntityDeathEvent events.
     *
     * @param event EntityDeathEvent instance
     */
    @SuppressWarnings("deprecation")
    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        Logger.debug("Death {");
        Player damager = event.getEntity().getKiller();
        LivingEntity victim = event.getEntity();
        if (victim instanceof Player) {
            if (((Player) victim).getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            Logger.debug("  PvP;");
            if (CustomEnchants.getVersion().equals("1.8")) {
                EventManager.callPlayerMDeath(damager, victim, event, damager.getItemInHand());
            } else {
                EventManager.callPlayerMDeath(damager, victim, event, damager.getInventory().getItemInMainHand());
                EventManager.callPlayerMDeath(damager, victim, event, damager.getInventory().getItemInOffHand());
            }
            EventManager.callPlayerDeath(damager, (Player) victim, event);

        } else {
            Logger.debug("  PvE;");
            if (CustomEnchants.getVersion().equals("1.8")) {
                EventManager.callPlayerMDeath(damager, victim, event, damager.getItemInHand());
            } else {
                EventManager.callPlayerMDeath(damager, victim, event, damager.getInventory().getItemInMainHand());
                EventManager.callPlayerMDeath(damager, victim, event, damager.getInventory().getItemInOffHand());
            }
        }
        Logger.debug("}");
    }
}