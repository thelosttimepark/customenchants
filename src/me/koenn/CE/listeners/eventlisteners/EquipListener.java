package me.koenn.CE.listeners.eventlisteners;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.listeners.EventManager;
import org.bukkit.Bukkit;

public class EquipListener {

    @SuppressWarnings("deprecation")
    public static void startEquipListener() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(CustomEnchants.instance, () -> {
            if (CustomEnchants.getVersion().equals("1.8")) {
                Bukkit.getOnlinePlayers().forEach(player -> EventManager.callArmorEquipped(player, player.getItemInHand()));
            } else {
                Bukkit.getOnlinePlayers().forEach(player -> EventManager.callArmorEquipped(player, player.getInventory().getItemInMainHand()));
                Bukkit.getOnlinePlayers().forEach(player -> EventManager.callArmorEquipped(player, player.getInventory().getItemInOffHand()));
            }
        }, 0, 20);

    }
}
