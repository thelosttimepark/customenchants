package me.koenn.CE.listeners.blocklisteners;

import me.koenn.CE.listeners.EnchantListener;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

public interface BreakListener extends EnchantListener {

    void onBlockBreak(BlockBreakEvent event, Player breaker, Block broken, int level);
}
