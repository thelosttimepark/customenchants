package me.koenn.CE.references;

import me.koenn.CE.enchantments.EnchantType;
import me.koenn.CE.util.Tool;

/**
 * Information and settings for all the different enchantments.
 */
public class Enchantments {

    public static class Poison {
        public static String NAME = "Venom Bite";
        public static String DESC = "Has a chance of giving your%victim poison";
        public static int MAX_LEVEL = 6;
        public static Tool TOOL = Tool.SWORD;
        public static EnchantType TYPE = EnchantType.RARE;
    }

    public static class Wither {
        public static String NAME = "Withering";
        public static String DESC = "Has a chance of giving your%victim wither";
        public static int MAX_LEVEL = 4;
        public static Tool TOOL = Tool.MELEE;
        public static EnchantType TYPE = EnchantType.ULTRA_RARE;
    }

    public static class Lightning {
        public static String NAME = "Electric Rage";
        public static String DESC = "Has a chance to let lightning%strike your victim";
        public static int MAX_LEVEL = 4;
        public static Tool TOOL = Tool.MELEE;
        public static EnchantType TYPE = EnchantType.LEGENDARY;
    }

    public static class Experience {
        public static String NAME = "Flare of the Gods";
        public static String DESC = "Gives extra experience for %killing mobs";
        public static int MAX_LEVEL = 3;
        public static Tool TOOL = Tool.SWORD;
        public static EnchantType TYPE = EnchantType.LEGENDARY;
    }

    public static class Vampirism {
        public static String NAME = "Bite of the Vampire";
        public static String DESC = "Has a chance of lifesteal%when you hit someone";
        public static int MAX_LEVEL = 5;
        public static Tool TOOL = Tool.SWORD;
        public static EnchantType TYPE = EnchantType.LEGENDARY;
    }

    public static class Headless {
        public static String NAME = "Decapitate";
        public static String DESC = "Has a chance of getting the%skull of your victim";
        public static int MAX_LEVEL = 5;
        public static Tool TOOL = Tool.SWORD;
        public static EnchantType TYPE = EnchantType.COMMON;
    }

    public static class IceAspect {
        public static String NAME = "Resurrection of Time";
        public static String DESC = "Has a chance of giving your%victim slowness";
        public static int MAX_LEVEL = 3;
        public static Tool TOOL = Tool.AXE;
        public static EnchantType TYPE = EnchantType.SIMPLE;
    }

    public static class Speed {
        public static String NAME = "Bless of Hermes";
        public static String DESC = "Gives you permanent%speed";
        public static int MAX_LEVEL = 4;
        public static Tool TOOL = Tool.BOOTS;
        public static EnchantType TYPE = EnchantType.LEGENDARY;
    }

    public static class Resistance {
        public static String NAME = "Spirit Barrage";
        public static String DESC = "Gives you permanent%resistance";
        public static int MAX_LEVEL = 2;
        public static Tool TOOL = Tool.ARMOR;
        public static EnchantType TYPE = EnchantType.ULTRA_RARE;
    }

    public static class Glowing {
        public static String NAME = "Bat Vision";
        public static String DESC = "Gives you permanent%night vision";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.HELMET;
        public static EnchantType TYPE = EnchantType.SIMPLE;
    }

    public static class AntiFire {
        public static String NAME = "Molten Salvo";
        public static String DESC = "Gives you permanent%fire resistance";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.ARMOR;
        public static EnchantType TYPE = EnchantType.RARE;
    }

    public static class Healing {
        public static String NAME = "Call of Greater Healing";
        public static String DESC = "Gives you an increased%regeneration rate";
        public static int MAX_LEVEL = 2;
        public static Tool TOOL = Tool.ARMOR;
        public static EnchantType TYPE = EnchantType.ULTRA_RARE;
    }

    public static class Shield {
        public static String NAME = "Fortitude Shield";
        public static String DESC = "Has a chance to completely%negate damage";
        public static int MAX_LEVEL = 3;
        public static Tool TOOL = Tool.ARMOR;
        public static EnchantType TYPE = EnchantType.COMMON;
    }

    public static class Lumber {
        public static String NAME = "Lumberjack";
        public static String DESC = "Chops down a whole%tree at once";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.AXE;
        public static EnchantType TYPE = EnchantType.RARE;
    }

    public static class Saturation {
        public static String NAME = "Feeding";
        public static String DESC = "Never take hunger";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.HELMET;
        public static EnchantType TYPE = EnchantType.RARE;
    }

    public static class Haste {
        public static String NAME = "Quickdraw";
        public static String DESC = "Gives you permanent%haste";
        public static int MAX_LEVEL = 4;
        public static Tool TOOL = Tool.SWORD;
        public static EnchantType TYPE = EnchantType.ULTRA_RARE;
    }

    public static class AutoRepair {
        public static String NAME = "Blacksmith";
        public static String DESC = "Automaticly repairs your%tool/armor";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.ANY;
        public static EnchantType TYPE = EnchantType.RARE;
    }

    public static class Blindness {
        public static String NAME = "Face Cover";
        public static String DESC = "Has a chance of giving your%victim blindness";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.MELEE;
        public static EnchantType TYPE = EnchantType.COMMON;
    }

    public static class MiningFatigue {
        public static String NAME = "Panic Attack";
        public static String DESC = "Has a chance of giving your%victim mining fatigue";
        public static int MAX_LEVEL = 2;
        public static Tool TOOL = Tool.MELEE;
        public static EnchantType TYPE = EnchantType.SIMPLE;
    }

    public static class Cure {
        public static String NAME = "Cure";
        public static String DESC = "Removes Mining Fatigue, Nausea and Blindness";
        public static int MAX_LEVEL = 1;
        public static Tool TOOL = Tool.ARMOR;
        public static EnchantType TYPE = EnchantType.ULTRA_RARE;
    }

    public static class JumpBoost {
        public static String NAME = "High Jump";
        public static String DESC = "Gives you permanent%jump boost";
        public static int MAX_LEVEL = 4;
        public static Tool TOOL = Tool.BOOTS;
        public static EnchantType TYPE = EnchantType.RARE;
    }
}
