package me.koenn.CE.gui;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.util.BookUtil;
import me.koenn.core.gui.Gui;
import me.koenn.core.gui.Option;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class EnchantsGui extends Gui {

    public EnchantsGui(Player player) {
        super(player, (String) CustomEnchants.Settings.guiSettings.getFromBody("ENCHANT_LIST"), Gui.getRequiredRows(CustomEnchants.getEnchantmentRegistry().getEnchantCount()));
        for (CustomEnchantment enchantment : CustomEnchants.getEnchantmentRegistry().getRegisteredEnchantments()) {
            ItemStack book = BookUtil.getBook(enchantment, enchantment.getMaxLevel(), 0, 100);
            this.addOption(new Option(book, i -> {
                if (player.isOp()) {
                    player.getInventory().addItem(book);
                }
            }));
        }
    }
}
