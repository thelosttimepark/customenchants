package me.koenn.CE.gui;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.enchantments.EnchantType;
import me.koenn.CE.util.BookUtil;
import me.koenn.CE.util.ExpUtil;
import me.koenn.core.gui.Gui;
import me.koenn.core.gui.Option;
import me.koenn.core.misc.ItemHelper;
import me.koenn.core.misc.LoreHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class EnchanterGui extends Gui {

    public EnchanterGui(Player player) {
        super(player, (String) CustomEnchants.Settings.guiSettings.getFromBody("ENCHANTER"));
        this.setOption(2, new Option(ItemHelper.makeItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7, EnchantType.SIMPLE.getColor() + EnchantType.SIMPLE.toString() + " Enchantment Book", LoreHelper.makeLore(ChatColor.YELLOW + "" + CustomEnchants.expCost[0] + "xp")), i -> {
            if (ExpUtil.getTotalExperience(player) < CustomEnchants.expCost[0]) {
                player.sendMessage(ChatColor.RED + "You do not have enough exp to buy this!");
                return;
            }
            ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) - CustomEnchants.expCost[0]);
            player.getInventory().addItem(BookUtil.getBook(BookUtil.getRandomBook(EnchantType.SIMPLE)));
        }));

        this.setOption(3, new Option(ItemHelper.makeItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0, EnchantType.COMMON.getColor() + EnchantType.COMMON.toString() + " Enchantment Book", LoreHelper.makeLore(ChatColor.YELLOW + "" + CustomEnchants.expCost[1] + "xp")), i -> {
            if (ExpUtil.getTotalExperience(player) < CustomEnchants.expCost[1]) {
                player.sendMessage(ChatColor.RED + "You do not have enough exp to buy this!");
                return;
            }
            ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) - CustomEnchants.expCost[1]);
            player.getInventory().addItem(BookUtil.getBook(BookUtil.getRandomBook(EnchantType.COMMON)));
        }));

        this.setOption(4, new Option(ItemHelper.makeItemStack(Material.STAINED_GLASS_PANE, 1, (short) 3, EnchantType.RARE.getColor() + EnchantType.RARE.toString() + " Enchantment Book", LoreHelper.makeLore(ChatColor.YELLOW + "" + CustomEnchants.expCost[2] + "xp")), i -> {
            if (ExpUtil.getTotalExperience(player) < CustomEnchants.expCost[2]) {
                player.sendMessage(ChatColor.RED + "You do not have enough exp to buy this!");
                return;
            }
            ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) - CustomEnchants.expCost[2]);
            player.getInventory().addItem(BookUtil.getBook(BookUtil.getRandomBook(EnchantType.RARE)));
        }));

        this.setOption(5, new Option(ItemHelper.makeItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5, EnchantType.ULTRA_RARE.getColor() + EnchantType.ULTRA_RARE.toString().replace("_", " ") + " Enchantment Book", LoreHelper.makeLore(ChatColor.YELLOW + "" + CustomEnchants.expCost[3] + "xp")), i -> {
            if (ExpUtil.getTotalExperience(player) < CustomEnchants.expCost[3]) {
                player.sendMessage(ChatColor.RED + "You do not have enough exp to buy this!");
                return;
            }
            ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) - CustomEnchants.expCost[3]);
            player.getInventory().addItem(BookUtil.getBook(BookUtil.getRandomBook(EnchantType.ULTRA_RARE)));
        }));

        this.setOption(6, new Option(ItemHelper.makeItemStack(Material.STAINED_GLASS_PANE, 1, (short) 1, EnchantType.LEGENDARY.getColor() + EnchantType.LEGENDARY.toString() + " Enchantment Book", LoreHelper.makeLore(ChatColor.YELLOW + "" + CustomEnchants.expCost[4] + "xp")), i -> {
            if (ExpUtil.getTotalExperience(player) < CustomEnchants.expCost[4]) {
                player.sendMessage(ChatColor.RED + "You do not have enough exp to buy this!");
                return;
            }
            ExpUtil.setTotalExperience(player, ExpUtil.getTotalExperience(player) - CustomEnchants.expCost[4]);
            player.getInventory().addItem(BookUtil.getBook(BookUtil.getRandomBook(EnchantType.LEGENDARY)));
        }));
    }
}
