package me.koenn.CE.commands;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.gui.EnchantsGui;
import me.koenn.core.command.Command;
import me.koenn.core.gui.Gui;
import me.koenn.core.player.CPlayer;

public class Enchants extends Command {

    public Enchants() {
        super("enchants", "/enchants");
    }

    @Override
    public boolean execute(CPlayer cPlayer, String[] strings) {
        Gui gui = new EnchantsGui(cPlayer.getPlayer());
        Gui.registerGui(gui, CustomEnchants.instance);
        gui.open();
        return true;
    }
}
