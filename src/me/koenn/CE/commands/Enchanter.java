package me.koenn.CE.commands;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.gui.EnchanterGui;
import me.koenn.core.command.Command;
import me.koenn.core.gui.Gui;
import me.koenn.core.player.CPlayer;

public class Enchanter extends Command {

    public Enchanter() {
        super("enchanter", "/enchanter");
    }

    @Override
    public boolean execute(CPlayer cPlayer, String[] strings) {
        Gui gui = new EnchanterGui(cPlayer.getPlayer());
        Gui.registerGui(gui, CustomEnchants.instance);
        gui.open();
        return true;
    }
}
