package me.koenn.CE.util;

import org.bukkit.Sound;

public class SoundUtil {

    public static Sound ORB_PICKUP;
    public static Sound LEVEL_UP;
    public static Sound LAVA;
    public static Sound EXPLODE;

    public static void init(String bukkitVersion) {
        if (bukkitVersion.equals("1.8")) {
            ORB_PICKUP = Sound.valueOf("ORB_PICKUP");
            LEVEL_UP = Sound.valueOf("LEVEL_UP");
            LAVA = Sound.valueOf("LAVA");
            EXPLODE = Sound.valueOf("EXPLODE");
        } else {
            ORB_PICKUP = Sound.valueOf("ENTITY_EXPERIENCE_ORB_PICKUP");
            LEVEL_UP = Sound.valueOf("ENTITY_PLAYER_LEVELUP");
            LAVA = Sound.valueOf("BLOCK_LAVA_EXTINGUISH");
            EXPLODE = Sound.valueOf("ENTITY_GENERIC_EXPLODE");
        }
    }
}
