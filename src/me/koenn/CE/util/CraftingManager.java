package me.koenn.CE.util;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class CraftingManager {

    public static void registerRecipes() {
        ShapedRecipe recipe = new ShapedRecipe(getEnhancedTable()).shape("#$#", "%*%", "#%#");
        recipe.setIngredient('#', Material.GOLD_INGOT);
        recipe.setIngredient('%', Material.DIAMOND);
        recipe.setIngredient('*', Material.ENCHANTMENT_TABLE);
        recipe.setIngredient('$', Material.DIAMOND_BLOCK);
        Bukkit.addRecipe(recipe);
    }

    public static ItemStack getEnhancedTable() {
        ItemStack table = new ItemStack(Material.ENCHANTMENT_TABLE);
        ItemMeta meta = table.getItemMeta();
        meta.setDisplayName(ChatColor.WHITE + "Enhanced Enchantment Table");
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addEnchant(Enchantment.DURABILITY, 10, false);
        table.setItemMeta(meta);
        return table;
    }
}
