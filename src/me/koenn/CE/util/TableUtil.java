package me.koenn.CE.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

public class TableUtil {

    public static boolean isEnhancedTable(Block enchantmentTable) {
        Location location = enchantmentTable.getLocation();
        List<Location> locations = new ArrayList<>();
        locations.add(location.clone().add(2, 0, 2));
        locations.add(location.clone().add(2, 0, -2));
        locations.add(location.clone().add(-2, 0, 2));
        locations.add(location.clone().add(-2, 0, -2));
        for (Location shelf : locations) {
            if (!isBookshelfCorner(shelf.getBlock())) {
                return false;
            }
        }
        return true;
    }

    private static boolean isBookshelfCorner(Block shelf) {
        Location middle = shelf.getLocation();
        int amount = 0;
        List<Location> locations = new ArrayList<>();
        locations.add(middle.clone().add(1, 0, 0));
        locations.add(middle.clone().add(-1, 0, 0));
        locations.add(middle.clone().add(0, 0, 1));
        locations.add(middle.clone().add(0, 0, -1));
        for (Location loc : locations) {
            if (loc.getBlock().getType().equals(Material.BOOKSHELF)) {
                amount++;
            }
        }
        if (amount >= 2) {
            if (middle.clone().add(0, 1, 0).getBlock().getType().equals(Material.BOOKSHELF)) {
                return true;
            }
        }
        return false;
    }
}
