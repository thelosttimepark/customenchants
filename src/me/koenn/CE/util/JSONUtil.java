package me.koenn.CE.util;

import me.koenn.CE.CustomEnchants;
import me.koenn.CE.enchantments.CustomEnchantment;
import me.koenn.CE.enchantments.EnchantType;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JSONUtil {

    public static JSONObject getEnchantmentsJSON() {
        JSONObject object = new JSONObject();
        JSONArray enchantments = new JSONArray();
        for (CustomEnchantment enchantment : CustomEnchants.getEnchantmentRegistry().getRegisteredEnchantments()) {
            JSONObject enchantmentObject = new JSONObject();
            enchantmentObject.put("UUID", enchantment.getClass().getSimpleName());
            enchantmentObject.put("NAME", enchantment.getName());
            enchantmentObject.put("DESCRIPTION", enchantment.getDescription());
            enchantmentObject.put("MAX_LEVEL", String.valueOf(enchantment.getMaxLevel()));
            enchantmentObject.put("TOOL", enchantment.getTool().name());
            enchantmentObject.put("TIER", enchantment.getTier().name());
            enchantments.add(enchantmentObject);
        }
        object.put("ENCHANTMENTS", enchantments);
        return object;
    }

    public static void readEnchantmentsFromFile() {
        JSONArray enchantments = (JSONArray) CustomEnchants.Settings.enchantments.getFromBody("ENCHANTMENTS");
        for (Object object : enchantments) {
            JSONObject enchantment = (JSONObject) object;
            CustomEnchantment ench = CustomEnchants.getEnchantmentRegistry().getEnchantment((String) enchantment.get("UUID"));
            ench.setName(((String) enchantment.get("NAME")));
            ench.setDescription(((String) enchantment.get("DESCRIPTION")));
            ench.setMaxLevel(Integer.parseInt(((String) enchantment.get("MAX_LEVEL"))));
            ench.setTool(Tool.valueOf(((String) enchantment.get("TOOL")).toUpperCase()));
            ench.setTier(EnchantType.valueOf(((String) enchantment.get("TIER")).toUpperCase()));
        }
    }
}
