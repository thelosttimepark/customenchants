package me.koenn.CE.util;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public class Util {

    public static void breakTreeFrom(Block block, Player player) {
        Location main = block.getLocation();
        List<Block> wood = new ArrayList<>();
        wood.add(main.clone().add(1, 0, 0).getBlock());
        wood.add(main.clone().add(0, 0, 1).getBlock());
        wood.add(main.clone().add(-1, 0, 0).getBlock());
        wood.add(main.clone().add(0, 0, -1).getBlock());
        wood.add(main.clone().add(0, 1, 0).getBlock());
        wood.add(main.clone().add(0, -1, 0).getBlock());
        for (Block side : wood) {
            if (!side.getType().equals(Material.LOG) && !side.getType().equals(Material.LOG_2) && !side.getType().equals(Material.LEAVES) && !side.getType().equals(Material.LEAVES_2)) {
                continue;
            }
            if (side.getDrops().toArray().length > 0) {
                try {
                    Item item = block.getWorld().dropItem(side.getLocation().add(0.5, 0, 0.5), (ItemStack) side.getDrops().toArray()[0]);
                    item.setVelocity(item.getVelocity().zero());
                } catch (IndexOutOfBoundsException ex) {
                    player.getItemInHand().setDurability((short) (player.getItemInHand().getDurability() + 2));
                    side.setType(Material.AIR);
                    breakTreeFrom(side, player);
                    continue;
                }
            }
            player.getItemInHand().setDurability((short) (player.getItemInHand().getDurability() + 2));
            side.setType(Material.AIR);
            breakTreeFrom(side, player);
        }
    }

    public static void applyEffect(PotionEffect potionEffect, Player player) {
        if (!player.hasPotionEffect(potionEffect.getType())) {
            player.addPotionEffect(potionEffect);
        } else {
            player.addPotionEffect(potionEffect, true);
        }
    }

    public static void strikeLightning(LivingEntity el, int level) {
        World world = el.getWorld();
        Location location = el.getLocation();
        world.strikeLightningEffect(location);
        el.damage(level / 2);
        el.setFireTicks(50 * (level));
    }

    public static void getSkull(Player el) {
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(el.getName());
        meta.setDisplayName(ChatColor.DARK_PURPLE + "Skull of: " + el.getName());
        skull.setItemMeta(meta);
        el.getWorld().dropItemNaturally(el.getLocation().add(0, 0.2, 0), skull);
    }
}
