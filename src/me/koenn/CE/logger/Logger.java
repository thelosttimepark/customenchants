package me.koenn.CE.logger;

import me.koenn.CE.CustomEnchants;
import org.bukkit.Bukkit;

public class Logger {

    public static void info(String message) {
        Bukkit.getLogger().info(String.format("[%s] %s", CustomEnchants.instance.getName(), message));
    }

    public static void debug(String message) {
        if (CustomEnchants.isInDebugMode()) {
            Bukkit.getLogger().info(String.format("[%s] [DEBUG] %s", CustomEnchants.instance.getName(), message));
        }
    }
}
