package me.koenn.CE.effects;

import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.AnimatedBallEffect;
import de.slikey.effectlib.effect.LineEffect;
import de.slikey.effectlib.effect.SphereEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;
import me.koenn.CE.CustomEnchants;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Effect to show when you enchant a book.
 */
public class EnchantEffect {

    private AnimatedBallEffect effect;
    private List<Location> locations;
    private EffectManager manager;
    private Location exact;

    /**
     * Constructs the effect, and prepares everything to play it.
     *
     * @param enchantmentTable The enchantTable to play the effect on.
     */
    public EnchantEffect(Block enchantmentTable) {
        exact = enchantmentTable.getLocation().add(0.5, 0.9, 0.5);
        manager = new EffectManager(CustomEnchants.instance);
        this.effect = setupEffect(new AnimatedBallEffect(manager));
        this.effect.setDynamicOrigin(new DynamicLocation(exact));
        this.effect.setDynamicTarget(new DynamicLocation(exact));

        locations = new ArrayList<>();
        locations.add(exact.clone().add(2, 1.5, 2));
        locations.add(exact.clone().add(2, 1.5, -2));
        locations.add(exact.clone().add(-2, 1.5, 2));
        locations.add(exact.clone().add(-2, 1.5, -2));
    }

    private AnimatedBallEffect setupEffect(AnimatedBallEffect effect) {
        effect.iterations = 25;
        effect.size = 0.5F;
        effect.yFactor = 1.0F;
        effect.yOffset = 0.0F;
        effect.particles = 100;
        return effect;
    }

    /**
     * Play the effect.
     */
    public void play() {
        this.effect.start();
        List<Item> items = new ArrayList<>();
        for (Location location : this.locations) {
            Item item = location.getWorld().dropItem(location.getBlock().getLocation().add(0.5, 0.2, 0.5), new ItemStack(Material.BOOK));
            item.setVelocity(item.getVelocity().zero());
            item.setPickupDelay(Integer.MAX_VALUE);
            items.add(item);

            LineEffect effect = new LineEffect(this.manager);
            effect.particle = ParticleEffect.ENCHANTMENT_TABLE;
            effect.speed = 0.2F;
            effect.particles = 20;
            effect.iterations = 10;
            effect.setDynamicOrigin(new DynamicLocation(location));
            effect.setDynamicTarget(new DynamicLocation(this.exact));
            effect.start();
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(CustomEnchants.instance, () -> {
            items.forEach(org.bukkit.entity.Item::remove);
            for (Item item : items) {
                EffectManager manager = new EffectManager(CustomEnchants.instance);
                SphereEffect effect = new SphereEffect(manager);
                effect.iterations = 2;
                effect.particles = 25;
                effect.particle = ParticleEffect.SMOKE_NORMAL;
                effect.speed = 0.1F;
                effect.setDynamicOrigin(new DynamicLocation(item.getLocation()));
                effect.setDynamicTarget(new DynamicLocation(item.getLocation()));
                effect.start();
            }
        }, 50);
    }
}
