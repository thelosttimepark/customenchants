package me.koenn.CE.effects;

import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.SphereEffect;
import de.slikey.effectlib.util.DynamicLocation;
import de.slikey.effectlib.util.ParticleEffect;
import me.koenn.CE.CustomEnchants;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * Effect to show.
 */
@Deprecated
public class TableEffect {

    private SphereEffect effect;
    private int task;

    /**
     * Constructs the effect, and prepares everything to play it.
     *
     * @param enchantmentTable The enchantTable to play the effect on.
     */
    public TableEffect(Block enchantmentTable) {
        Location exact = enchantmentTable.getLocation().add(0.5, 0.5, 0.5);
        EffectManager manager = new EffectManager(CustomEnchants.instance);
        this.effect = new SphereEffect(manager);
        this.effect.particle = ParticleEffect.ENCHANTMENT_TABLE;
        this.effect.iterations = 5;
        this.effect.radius = 2.2;
        this.effect.speed = 0.2F;
        this.effect.particles = 200;
        this.effect.setDynamicOrigin(new DynamicLocation(exact));
        this.effect.setDynamicTarget(new DynamicLocation(exact));
    }

    /**
     * Play the effect.
     */
    public void play() {
        this.effect.start();
    }
}

